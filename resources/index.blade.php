<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Web Starter Kit">
    <meta name="theme-color" content="#2F3BA2">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

    <title>Image Picker</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="/styles/main.css">
</head>
<body>


<section id="app">

</section>
<footer>

</footer>


{{--//////***** SCRIPTS *****///////--}}
<script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous">
</script>

{{--<script src="/bower_components/jquery/dist/jqueryAjax.js"></script>--}}
<script src="/bower_components/caman/dist/caman.full.min.js"></script>
<script src="/bower_components/jq-ajax-progress/src/jq-ajax-progress.min.js"></script>
<script src="/js/main.js"></script>


</body>
</html>